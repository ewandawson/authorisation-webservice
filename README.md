# Authorisation Webservice #

## Goal ##

Construct a web service with a single endpoint that will accept a JSON request and respond with an authorise/deny decision.

The challenge is not to create a perfect, complete solution - there may not be enough time to implement the entire specification. The idea is to evaluate your approach to creating a web service to solve a business problem. In particular, I'm interested in your abilities in the following areas:

* Understanding a specification
* Designing a software solution using a proper object-oriented approach
* Designing for testability
* Designing for maintainability

You are free to use any resources at your disposal to solve this problem. In particular, if there are any frameworks/libraries that you want to use, feel free to do so. The only stipulation is that I must be able to check-out, build and run your solution on my computer.

After a couple of hours or so, we will look at your solution together and discuss your approach and the design decisions you have made.

If you have any questions, either now or during the exercise, please don't hesitate to ask me.

## Specification ##

The web service should accept HTTP POST requests at [http:localhost:8080/authorise](http://localhost:8080/authorise). It should accept a JSON-formatted payload in the body of the request. The JSON payload will consist of key-value pairs, with values of either arbitrary strings, or integers. An example payload is given below:

``` JSON
{
  "string1" : "string value",
  "string2" : "another@string.value.com",
  "integer1" : 100,
  "integer2" : -1
}
```

The web service should then apply one or more authorisation rules to this request payload. Each rule operates on a map of key-value pairs, and returns a permit/deny response. The web service then aggregates these decisions and produces an overall decision of permit or deny. If the overall decision is "permit", then the web service should respond with an HTTP status 200 response. If the overall decision is "deny", then the web service should respond with an HTTP status 403 response.

### Authorisation Rules ###

The web service should apply three different types of authorisation rule to the request:

* For a given key, does the value match a regular expression?
* For a given key, is the integer value greater than some integer?
* For a given key, is the integer value less than some integer?
* Does a given key exitst in the request key-value map?

If the web service is configured to use more than one rule, there needs to be a combining algorithm to produce a single aggregate authorisation decision. The web service should support the following combining algorithms:

* DENY OVERRIDES PERMIT: If any of the rule decisions is DENY, then the overall decision is DENY
* PERMIT OVERRIDES DENY: If any of the rule decisions is PERMIT, then the overall decision is PERMIT
* MAJORITY WINS, PERMIT ON TIE
* MAJORITY WINS, DENY ON TIE

### Configuration ###

The web service needs to be configured with a set of rules and a combining algorithm to use. The configuration should be read from a file located at C:\auth-webservice-config.<ext>. You can use any format you wish for the configuration file, and may use any file extension.